# FILE NAME:	utils.py
#
# PURPOSE:	Utilities - state/cron manupulation, etc.
#
# FILE REFERENCES:
#
# Name		IO	Description
# ----		--	-----------
# crons.json	IO	Cronjob info file. Full path provided by system arguments.
# state.json	IO	State info file. Full path provided by system arguments.
#
# EXTERNAL VARIABLES:
#
# Source: header.py
#
# Name		Type	IO	Description
# ----		----	--	-----------
# WRITE_LOCK	str	I	Write lock location.
#
# EXTERNAL REFERENCES:
#
# Name			Description
# ----			-----------
# logging		Logging Handler.
# json			Json encoder & decoder.
# log_error_and_exit	Fatal log and exit handler.
# os.path		Used for checking if file exists.
# FileLock		Locks a file, and prevents it from being written on twice.
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES:
#	- Invalid Arguments
#	- Invalid file references
#
# NOTES: none
#
# ASSUMPTIONS, CONDITIONS, RESTRICTIONS:
#	- valid file references.

import logging
import json
import os.path
from logger import log_error_and_exit
from commands.header import WRITE_LOCK

try:
	from filelock import FileLock
except:
	FileLock = None
	log_error_and_exit(
			Exception('Critical dependency missing - filelock.'),
			'Unable to continue, missing filelock.',
			1)

#
# State Stuff
#


def check_statefile(path, log_origin):
	'''
	FUNCTION NAME: check_statefile

	PURPOSE: Check state file path. If the path is invalid, return False.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	path		str	Path to state.json.
	log_origin	str	Log origin: function of origin. (full path)

	RETURN VALUE: (bool) True if argument is valid and file exists.

	NOTES: Fatal exit if the path type or length is invalid.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	----------
	# checked_log_origin	str	Validated log origin.


	# WARNING: If log_origin is not of type str.
	# Then checked_log_origin will change type to str.
	checked_log_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/modules/utils.py, check_statefile: log_origin is invalid.')
		checked_log_origin = 'unknown'

	if type(path) != str or len(path) <= 0:
		log_error_and_exit(Exception(
				'Empty statefile path.'
			),
			'{}: statefile not defined.'.format(checked_log_origin),
			1)

	if not os.path.isfile(path):
		return False

	return True


def write_state_file(path, state):
	'''
	FUNCTION NAME: write_state_file

	PURPOSE: Write the new state data to state.json.

	ARGUMENT LIST;

	Argument	Type	Description
	--------	----	-----------
	state		dict	State data.
	path		str	Path to state.json.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type		Description
	# --------	----		-----------
	# state_file	file_stream	Crons file stream.

	if type(path) != str or len(path) <= 0:
		log_error_and_exit(Exception(
			'Failed to write state.json file'),
			'commands/modules/utils.py, write_state_file: received invalid path {}'.format(path),
			1)
	if type(state) != dict:
		log_error_and_exit(Exception(
			'Failed to write state.json file.'
			),
			'commands/modules/utils.py, write_state_file: received an invalid state', 1)
	try:
		lock = FileLock(WRITE_LOCK, timeout=10)
		with lock:
			state_file = open(path, 'w')
			json.dump(state, state_file)
	except Exception as e:
		log_error_and_exit(Exception('Could not write state file: {}'.format(e)),
				'commands/modules/utils.py, write_state_file: {}'.format(e), 1)


def load_state_file(path):
	'''
	FUNCTION NAME: load_state_file

	PURPOSE: Load the state data from state.json.

	ARGUMENT LIST;

	Argument	Type	Description
	--------	----	-----------
	path		str	Path to state.json.

	RETURN VALUE: (dict) state
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type		Description
	# --------	----		-----------
	# state_file	file_stream	Crons file stream.

	try:
		with open(path) as state_file:
			return json.load(state_file)
	except Exception as e:
		log_error_and_exit(Exception('Invalid state file: {}'.format(e)),
				'commands/pin.py, pin: {}'.format(e), 1)

	return 0


#
# Cron Stuff
#


def check_cronfile(path, log_origin):
	'''
	FUNCTION NAME: check_cronfile

	PURPOSE: Check cronfile path. If the path is invalid, return False.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	path		str	Path to cron.json.
	log_origin	str	Log origin: function of origin. (full path)

	RETURN VALUE: (bool) True if argument is valid and file exists.

	NOTES: Fatal exit if the path type or length is invalid.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	----------
	# checked_log_origin	str	Validated log origin.


	# WARNING: If log_origin is not of type str.
	# Then checked_log_origin will change type to str.
	checked_log_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/modules/utils.py, check_cronfile: log_origin is invalid.')
		checked_log_origin = 'unknown'

	if type(path) != str or len(path) <= 0:
		log_error_and_exit(Exception(
				'Empty cronfile path.'
			),
			'{}: cronfile not defined.'.format(checked_log_origin),
			1)

	if not os.path.isfile(path):
		return False

	return True


def invalid_crons_die(log_origin):
	'''
	FUNCTION NAME: invalid_cron_die

	PURPOSE: Fatal exit. Used if crons is invalid.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	log_origin	str	Log Origin: Function of origin. (full path)
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	----------
	# checked_log_origin	str	Validated log origin.


	# WARNING: If log_origin is not of type str.
	# Then checked_log_origin will change type to str.
	checked_log_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/modules/utils.py, invalid_crons_die: log_origin is invalid.')
		checked_log_origin = 'unknown'

	log_error_and_exit(Exception(
		'Failed to use crons. This could due to an invalid cron.json.'),
		'{}: invalid cron.json data.'.format(checked_log_origin),
		1)


def get_cron_index(crons, pin, cron):
	'''
	FUNCTION NAME: get_cron_index

	PURPOSE: Gets the cronjob index.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	crons		dict	Cron data from crons.json.
	pin		int	Pin number.
	cron		str	Cron statement.

	RETURN VALUE: (int) index. -1 if not found.
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# pin_crons	list	List of the pin's crons.

	if check_pin_number(pin, 'cron_exists', hard_exit=False) == False:
		return -1
	if type(crons) != dict or crons == {}:
		logging.error('commands/modules/utils.py, get_cron_index: Received invalid cron data.')
		return -1
	if type(cron) != str or len(cron) <= 0:
		logging.error('commands/modules/utils.py, get_cron_index: Received invalid cron statement.')
		return -1

	pin_status = find_pin_status(crons, pin)

	# Handle non existent pin
	if pin_status is None:
		logging.error('commands/modules/utils.py, get_cron_index: pin {} not found.'.format(pin))
		return -1

	if type(pin_status) != str:
		logging.error('commands/modules/utils.py, get_cron_index: find_pin_status returned an invalid status.')
		return -1

	pin_crons = crons[pin_status][str(pin)]['crons']
	for i, cron_loop_val in enumerate(pin_crons):
		if cron_loop_val['name'] == cron:
			return i

	return -1


def cron_exists(crons, pin, cron):
	'''
	FUNCTION NAME: cron_exists

	PURPOSE: Checks if a cronjob exists.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	crons		dict	Cron data from crons.json.
	pin		int	Pin number.
	cron		str	Cron statement.

	RETURN VALUE: (boolean)	True if cron exists for the given pin.
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# pin_crons	list	List of the pin's crons.

	if check_pin_number(pin, 'cron_exists', hard_exit=False) == False:
		return False
	if type(crons) != dict or crons == {}:
		logging.error('commands/modules/utils.py, cron_exists: Received invalid cron data.')
		return False
	if type(cron) != str or len(cron) <= 0:
		logging.error('commands/modules/utils.py, cron_exists: Received invalid cron statement.')
		return False

	pin_status = find_pin_status(crons, pin)

	# Handle non existent pin
	if pin_status is None:
		logging.error('commands/modules/utils.py, cron_exists: pin {} not found.'.format(pin))
		return False

	if type(pin_status) != str:
		logging.error('commands/modules/utils.py, cron_exists: find_pin_status returned an invalid status.')
		return False

	pin_crons = crons[pin_status][str(pin)]['crons']
	for cron_loop_val in pin_crons:
		if cron_loop_val['name'] == cron:
			return True

	return False


def find_pin_status(crons, pin):
	'''
	FUNCTION NAME: find_pin_status

	PURPOSE: Find pin status in crons.json. (Enabled or Disabled)

	ARGUMENT LIST;

	Argument	Type	Description
	--------	----	-----------
	crons		dict	Cron data.
	pin		int	Pin Number.

	RETURN VALUE: (str) 'enabled', 'disabled' or None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# none

	if type(crons) != dict or crons == {}:
		logging.error('commands/utils/utils.py, find_pin_status: Received invalid cron data.')
		return

	if check_pin_number(pin, 'find_pin_status', hard_exit=False) == False:
		logging.error('commands/utils/utils.py, find_pin_status: Received invalid pin.')
		return

	# Pin not found
	if (str(pin) not in crons['disabled'] and
			str(pin) not in crons['enabled']):
		logging.warn('commands/utils/utils.py, find_pin_status: Pin {} not found in crons.'.format(pin))
		return

	# Pin is enabled.
	if (str(pin) not in crons['disabled'] and
			str(pin) in crons['enabled']):
		return 'enabled'

	# Pin is already disabled.
	if (str(pin) not in crons['enabled'] and
			str(pin) in crons['disabled']):
		return 'disabled'


def check_pin_number(pin, log_origin, hard_exit=True):
	'''
	FUNCTION NAME: check_pin_number

	PURPOSE: Check pin number.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	pin		int	Pin to check.
	log_origin	str	Log origin: function of origin.
	hard_exit	bool	Exits with exit code 1.

	RETURN VALUE: (boolean) Pin Validity.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	----------
	# checked_log_origin	str	Validated log origin.


	# WARNING: If log_origin is not of type str.
	# Then checked_log_origin will change type to str.
	checked_log_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/pin.py, check_pin_number: log_origin is invalid.')
		checked_log_origin = 'unknown'

	if type(pin) != int or pin < 0 or pin > 40:
		if hard_exit == True:
			log_error_and_exit(Exception(
					'Invalid pin number. Choose one between 0 to 40.'
				),
				'commands/pin.py, {}: pin number out of bounds.'.format(checked_log_origin),
				1)
		else:
			logging.error('commands/pin.py, {}: pin number out of bounds'.format(checked_log_origin))
			return False

	return True


def load_cron_file(path):
	'''
	FUNCTION NAME: load_cron_file

	PURPOSE: Load cron file.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	path		str	File path to crons.json.

	RETURN VALUE: (dict) crons.json data.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type				Description
	# --------		----				----------
	# crons_file		file_stream			Crons file stream.
	# crons			dict				Crons data.

	crons = {}


	# Open and read cron file.
	try:
		with open(path) as crons_file:
			crons = json.load(crons_file)
	except Exception as e:
		log_error_and_exit(Exception('Invalid crons file: {}'.format(e)),
				'commands/modules/utils.py, load_cron_file: {}'.format(e), 1)

	if 'disabled' not in crons:
		log_error_and_exit(Exception('Invalid crons file.'),
				'commands/modules/utils.py, load_cron_file: crons.json missing disabled', 1)
	if 'enabled' not in crons:
		log_error_and_exit(Exception('Invalid crons file.'),
				'commands/modules/utils.py, load_cron_file: crons.json missing enabled', 1)

	return crons


def write_cron_file(path, crons):
	'''
	FUNCTION NAME: write_cron_file

	PURPOSE: Write the new cron data to crons.json.

	ARGUMENT LIST;

	Argument	Type	Description
	--------	----	-----------
	crons		dict	Cron data.
	path		str	Path to crons.json.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type		Description
	# --------	----		-----------
	# crons_file	file_stream	Crons file stream.

	if type(path) != str or len(path) <= 0:
		log_error_and_exit(Exception(
			'Failed to write crons.json file'),
			'commands/modules/utils.py, write_cron_file: received invalid path {}'.format(path),
			1)
	if type(crons) != dict or crons == {}:
		log_error_and_exit(Exception(
			'Failed to write crons.json file.'
			),
			'commands/modules/utils.py, write_cron_file: received invalid crons', 1)
	try:
		lock = FileLock(WRITE_LOCK, timeout=10)
		with lock:
			crons_file = open(path, 'w')
			json.dump(crons, crons_file)
	except Exception as e:
		log_error_and_exit(Exception('Could not write crons file: {}'.format(e)),
				'commands/modules/utils.py, write_cron_file: {}'.format(e), 1)
