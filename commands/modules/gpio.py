# FILE NAME:	gpio.py
#
# PURPOSE: Setup gpio pins as output and set initial values.
#
# FILE REFERENCES:
#
# Name	IO	Description
# ---	--	-----------
# none
#
# EXTERNAL VARIABLES:
#
# Name	Type	IO	DESCRIPTION
# ----	----	--	-----------
# none
#
# EXTERNAL REFERENCES:
#
# Name		DESCRIPTION
# ----		-----------
# RPi.GPIO	GPIO
# logging	Handles logging.
#
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGE: none
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS: none

import logging

try:
	import RPi.GPIO as GPIO
except:
	print('Dependency RPi.GPIO failed. Pin IO won\'t work. Check your depencies and permissions!')
	GPIO = None

def gpio_setup(state):
	'''
	FUNCTION NAME: gpio_setup

	PURPOSE: Initializes pins with the values found in state.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	state		dict	Pins state.

	RETURN VALUE: (bool) success status
	'''

	# LOCAL VARIABLES
	#
	# Variables	Type	Description
	# ---------	----	-----------
	# pin_state	bool	Pin state.

	if type(state) != dict or state == {}:
		logging.error('commands/modules/gpio.py, gpi_setup: An invalid state was provided. No pins have been setup.')
		return False

	if GPIO is None:
		logging.error('commands/modules/gpio.py, gpi_setup: Broken dependency Rpi.GPIO. No pins setup.')
	else:
		GPIO.setmode(GPIO.BCM)
		GPIO.setwarnings(False)

	pin_state = False
	for pin in state:
		pin_state = state[pin] == 'true'

		try:
			int(pin)
		except:
			logging.error('commands/modules/gpio.py, gpio_setup: invalid pin number in state.')
			return False

		if pin_state == True and GPIO is not None:
			GPIO.setup(int(pin), GPIO.OUT, initial=GPIO.HIGH)
		elif GPIO is not None:
			GPIO.setup(int(pin), GPIO.OUT, initial=GPIO.LOW)

	return True


def gpio_set_pin_low(pin, state):
	'''
	FUNCTION NAME: gpio_set_pin_low

	PURPOSE: Sets a pin to the low state.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	pin		int	Pin number.
	state		dict	Pin State.

	RETURN VALUE: (dict) state
	'''

	# LOCAL VARIABLES
	#
	# Variables		Type	Description
	# ---------		----	-----------
	# return_state		dict	State.

	return_state = state

	if type(state) != dict:
		logging.error('commands/modules/gpi.py, gpio_set_pin_low: Invalid state.')
		print('Pin changed in memory, gpio left intact.')
		return return_state

	if type(pin) != int or pin < 0 or pin > 40:
		logging.error('commands/modules/gpio.py, gpio_set_pin_low: Pin out of bounds.')
		print('Pin changed in memory, gpio left intact.')
		return return_state

	if GPIO is None:
		logging.error('commands/modules/gpio.py, gpio_set_pin_low: Broken dependency Rpi.GPIO. Proceeding as mock.')
	else:
		GPIO.output(pin, GPIO.LOW)

	return_state[str(pin)] = 'false'

	return return_state


def gpio_set_pin_high(pin, state):
	'''
	FUNCTION NAME: gpio_set_pin_high

	PURPOSE: Sets a pin to the high state.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	pin		int	Pin number.
	state		dict	Pin State.

	RETURN VALUE: (dict) state
	'''

	# LOCAL VARIABLES
	#
	# Variables		Type	Description
	# ---------		----	-----------
	# return_state		dict	State.

	return_state = state

	if type(state) != dict:
		logging.error('commands/modules/gpi.py, gpio_set_pin_high: Invalid state.')
		print('Pin changed in memory, gpio left intact.')
		return return_state

	if type(pin) != int or pin < 0 or pin > 40:
		logging.error('commands/modules/gpio.py, gpio_set_pin_high: Pin out of bounds.')
		print('Pin changed in memory, gpio left intact.')
		return return_state

	if GPIO is None:
		logging.error('commands/modules/gpio.py, gpio_set_pin_high: Broken dependency Rpi.GPIO. Proceeding as mock.')
		print('Pin changed in memory, gpio left intact.')
	else:
		GPIO.output(pin, GPIO.HIGH)

	return_state[str(pin)] = 'true'

	return return_state
