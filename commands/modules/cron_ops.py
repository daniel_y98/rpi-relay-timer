# FILE NAME:	cron_ops.py
#
# PURPOSE: Install, remove and modify cronjobs.
#
# FILE REFERENCES:
#
# Name	IO	Description
# ---	--	-----------
# none
#
# EXTERNAL VARIABLES:
#
# Name	Type	IO	DESCRIPTION
# ----	----	--	-----------
# none
#
# EXTERNAL REFERENCES:
#
# Name		DESCRIPTION
# ----		-----------
# RPi.GPIO	GPIO
# logging	Handles logging.
#
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGE: none
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS: none

import logging

try:
	from crontab import CronTab
	from crontab import CronSlices
except:
	print('Dependency crontab failed. Cronjob installation, addition or removal won\'t work.')
	CronTab = None
	CronSlices = None

def install_cron(cron_statement, command):
	'''
	FUNCTION NAME: install_cron

	PURPOSE: Install cron job in system.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	cron_statement	str	Cron statement.
	command		str	Cron command.

	RETURN VALUE: (bool) success status
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type			Description
	# --------	----			-----------
	# cron		crontab.CronTab		Connection to cronfile.
	# job		crontab.CronItem	Cron job.

	if type(cron_statement) != str or len(cron_statement) <= 0:
		logging.error('commands/modules/cron_ops.py, install_cron: invalid cron_statement type.')
		return False

	if type(command) != str or len(command) <= 0:
		logging.error('crommands/modules/cron_ops.py, install_cron: invalid command type.')
		return False

	if CronTab is None:
		return True

	if CronSlices is None:
		return False

	if CronSlices.is_valid(cron_statement) == False:
		logging.erorr('Invalid job statement.')
		return False

	cron  = CronTab(user='root')
	job  = cron.new(command=command, comment='{} {}'.format(cron_statement, command))
	job.setall(cron_statement)

	cron.write()

	return True


def remove_cron(cron_statement, command):
	'''
	FUNCTION NAME: remove_cron

	PURPOSE: Remove cron job from system.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	cron_statement	str	Cron statement.
	command		str	Cron command.

	RETURN VALUE: (bool) success status
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type			Description
	# --------	----			-----------
	# cron		crontab.CronTab		Connection to cronfile.

	if type(command) != str or len(command) <= 0:
		logging.error('crommands/modules/cron_ops.py, remove_cron: invalid command type.')
		return False

	if CronTab is None:
		return True

	if CronSlices is None:
		return False

	if CronSlices.is_valid(cron_statement) == False:
		logging.erorr('Invalid job statement.')
		return False

	cron  = CronTab(user='root')
	if cron.remove_all(comment='{} {}'.format(cron_statement, command)) == 0:
		return False

	cron.write()

	return True
