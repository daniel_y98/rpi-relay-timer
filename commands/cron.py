# FILE NAME:	cron.py
#
# PURPOSE: Manage the crons.json file. This is the main file, which hosts
#		information about the pins and the cronjobs they have.
#
# FILE REFERENCES:
#
# Name		IO	Description
# ---		--	-----------
# crons.json	IO	Cronjob info file. Full path provided by system arguments.
#
# EXTERNAL VARIABLES:
#
# Source: header.py
#
# Name			Type		IO	DESCRIPTION
# ----			----		--	-----------
# COMMAND_CRON		str		I	Command string for setup.
# wrong_action_except	Exception	I	Wrong action exception.
# bad_parse_exept	Exception	I	Bad parse exception.
# RPI_INSTALL_NAME	str		I	Rpi timer installation name.
# CRON_LOCK		str		I	Location of the lock file for cron operations.
#
# EXTERNAL REFERENCES:
#
# Name			DESCRIPTION
# ----			----------
# argparse		Argument Parser.
# logging		Logging Handler.
# json			Json encoder & decoder.
# log_error_and_exit	Fatal log and exit handler.
# invalid_crons_die	Fatal exit if openning crons.json fails.
# get_cron_index	Gets the index of a given cron.
# find_pin_status	Gets the status of a pin.
# load_cron_file	Loads data from the crons.json file.
# write_cron_file	Write data to the crons.json file.
# check_cronfile	Checks the cronfile argument and exists fatally if the input is invalid.
# check_pin_number	Checks if a pin number is valid. Optional hard exit if it's invalid (default).
# install_cron		Install a given cronjob in the system.
# remove_cron		Remove a given cronjob from the system.
# FileLock		Locks a file, and prevents it from being written on twice.
#
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES:
#	- Invalid Arguments
#	- Invalid cron file.
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS:
#	- crons.json exists.

import argparse
import logging
import json
from logger import log_error_and_exit
from commands.header import COMMAND_CRON, wrong_action_except
from commands.header import bad_parse_exept
from commands.header import RPI_INSTALL_NAME, CRON_LOCK
from commands.modules.utils import invalid_crons_die, get_cron_index
from commands.modules.utils import cron_exists, find_pin_status
from commands.modules.utils import load_cron_file, write_cron_file
from commands.modules.utils import check_cronfile, check_pin_number
from commands.modules.cron_ops import install_cron, remove_cron

try:
	from filelock import FileLock
except:
	FileLock = None
	log_error_and_exit(
			Exception('Critical dependency missing - filelock.'),
			'Unable to continue, missing filelock.',
			1)

def cron(args):
	'''
	FUNCTION NAME: crons

	PURPOSE: Handles the cron command.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# Check arguments
	check_args(args, 'cron')

	if args.command != COMMAND_CRON:
		log_error_and_exit(Exception(
			'Wrong command handler.'
		),
		'commands/cron.py, cron: wrong command handler.',
		1)

	check_pin_number(args.pin, 'commands/cron.py, cron') # External function, requires an absolute log origin.

	if args.action == 'init':
		init(args)
	elif args.action == 'rename':
		rename(args)
	elif args.action == 'enable':
		enable(args)
	elif args.action == 'disable':
		disable(args)
	elif args.action == 'add':
		add(args)
	elif args.action == 'remove':
		remove(args)
	elif args.action == 'edit':
		edit(args)
	else:
		log_error_and_exit(Exception(
			'No handler for action {}.'.format(args.action)
			),
			'commands/crons.py, crons: no catch for {}.'.format(args.action), 1)


#
# ACTIONS
#


def edit(args):
	'''
	FUNCTION NAME: edit

	PURPOSE: Edit cronjob action.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Argument.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# crons			dict	Data from crons.json
	# pin_status		str	Pin Status, (enabled, disabled)
	# cron_available	bool	True if the cron is available.
	# cron_index		int	Cron index.
	# cron_data		dict	Cron Data.
	# cron_output		str	Cron output. True or False. High or Low.

	crons = {}

	# Check argument
	check_args(args, 'edit')

	if args.action != 'edit':
		log_error_and_exit(wrong_action_except,
				'commands/cron.py, edit: wrong action handler.',
				1)

	# Check required arguments
	check_cron(args.cron, 'edit')
	check_output(args.output, 'edit')
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, edit')
	check_cronfile(args.cronfile, 'commands/cron.py, edit')

	# Log unused arguments
	log_name_arg_unsed(args.name, 'edit')

	logging.debug('commands/crons.py, edit: Openning crons file.')
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/crons.py, edit')

		pin_status = find_pin_status(crons, args.pin)
		
		if pin_status is None:
			pin_not_found_die(args.pin, 'NAME', 'edit')

		if type(pin_status) != str:
			logger.error('command/cron.py, edit: find_pin_status returned an invalid type.')
			pin_not_found_die(args.pin, args.name, 'edit')

		if pin_status == 'disabled':
			log_error_and_exit(Exception(
				'Pin is diabled. To enable it run: cron enable --pin {}'.format(args.pin)
				),
				'commands/crons.py, edit: Pin is disabled.',
				1)

		cron_available = cron_exists(crons, args.pin, args.cron)

		if type(cron_available) != bool or cron_available == False:
			print('Cron not found. Nothing to do.')
			return

		cron_index = get_cron_index(crons, args.pin, args.cron)
		if type(cron_index) != int or cron_index <= -1:
			print('Cron not found. Nothing to do.')
			return

		cron_data = crons[pin_status][str(args.pin)]['crons'][cron_index]
		cron_output = 'high' if cron_data['action'] == True else 'low'
		remove_cron(cron_data['name'], '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, args.pin, cron_output))

		
		if install_cron(args.cron, '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, args.pin, args.output)) == False:
			log_error_and_exit(
					Exception('Could not add cronjob. Bad syntax?'),
					'commands/cron.py, add: bad status from install_cron.',
					1
					)

		crons[pin_status][str(args.pin)]['crons'][cron_index]['action'] = args.output == 'high'

		logging.debug('commands/crons.py, edit: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Removed cron {} for pin {}.'.format(args.cron, args.pin))


def remove(args):
	'''
	FUNCTION NAME: remove

	PURPOSE: Remove a cronjob from pin.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''
	
	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# crons			dict	Data from crons.json
	# pin_status		str	Pin Status. (enabled, disabled)
	# cron_available	bool	True if the cron is available.
	# cron_index		int	Cron index.
	# cron_data		dict	Cron Data.
	# cron_output		str	Cron output. True or False. High or Low.

	crons = {}

	# Check arguments
	check_args(args, 'remove')

	if args.action != 'remove':
		log_error_and_exit(wrong_action_except,
				'commands/cron.py, remove: wrong action handler.',
				1)

	# Check required arguments
	check_cron(args.cron, 'remove')
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, remove')
	check_cronfile(args.cronfile, 'commands/cron.py, remove')

	# Log unused arguments
	log_name_arg_unsed(args.name, 'remove')
	log_output_arg_unused(args.name, 'remove')

	logging.debug('commands/crons.py, remove: Openning crons file.')
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/cron.py, remove')

		pin_status = find_pin_status(crons, args.pin)
		
		if pin_status is None:
			pin_not_found_die(args.pin, 'NAME', 'remove')

		if type(pin_status) != str:
			logger.error('command/cron.py, remove: find_pin_status returned an invalid type.')
			pin_not_found_die(args.pin, args.name, 'remove')

		if pin_status == 'disabled':
			log_error_and_exit(Exception(
				'Pin is diabled. To enable it run: cron enable --pin {}'.format(args.pin)
				),
				'commands/crons.py, remove: Pin is disabled.',
				1)

		cron_available = cron_exists(crons, args.pin, args.cron)

		if type(cron_available) != bool or cron_available == False:
			print('Cron not found. Nothing to do.')
			return

		cron_index = get_cron_index(crons, args.pin, args.cron)
		if type(cron_index) != int or cron_index <= -1:
			print('Cron not found. Nothing to do.')
			return

		cron_data = crons[pin_status][str(args.pin)]['crons'][cron_index]
		cron_output = 'high' if cron_data['action'] == True else 'low'

		if remove_cron(str(cron_data['name']), '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, args.pin, cron_output)) == False:
				log_error_and_exit(
					Exception(
						'Failed to remove cronjob.'
						),
						'commands/cron.py, remove: remove_cron returned a bad status.',
						1
					)

		crons[pin_status][str(args.pin)]['crons'].pop(cron_index)

		logging.debug('commands/crons.py, remove: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Removed cron {} for pin {}.'.format(args.cron, args.pin))


def add(args):
	'''
	FUNCTION NAME: add

	PURPOSE: Adds a cronjob to pin.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Varible	Type	Description
	# -------	----	-----------
	# crons		dict	Data from crons.json.
	# pin_status	str	Pin Status.
	# pin_crons	list	List of crons for pin.

	crons = {}

	# Check arguments
	check_args(args, 'add')

	if args.action != 'add':
		log_error_and_exit(wrong_action_handler,
				'commands/cron.py, add: wrong action handler.',
				1)

	# Check required arguments
	check_cron(args.cron, 'add')
	check_output(args.output, 'add')
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, add')
	check_cronfile(args.cronfile, 'commands/cron.py, add')

	# Log unused arguments
	log_name_arg_unsed(args.name, 'add')

	logging.debug('commands/crons.py, add: Openning crons file.')
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/cron.py, add')

		pin_status = find_pin_status(crons, args.pin)

		if pin_status is None:
			pin_not_found_die(args.pin, 'NAME', 'add')

		if type(pin_status) != str:
			logger.error('command/cron.py, add: find_pin_status returned an invalid type.')
			pin_not_found_die(args.pin, args.name, 'add')

		if pin_status == 'disabled':
			log_error_and_exit(Exception(
				'Pin is diabled. To enable it run: cron enable --pin {}'.format(args.pin)
				),
				'commands/crons.py, add: Pin is disabled.',
				1)

		# Check if cron was already added.
		pin_crons = crons[pin_status][str(args.pin)]['crons']
		for cron in pin_crons:
			if cron['name'] == args.cron:
				print('Cronjob exists')
				logging.info('commands/cron.py, add: cron exists.')
				return

		if pin_status == 'enabled':
			if install_cron(args.cron, '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, args.pin, args.output)) == False:
				log_error_and_exit(
						Exception('Could not add cronjob. Bad syntax?'),
						'commands/cron.py, add: bad status from install_cron.',
						1
						)

		crons[pin_status][str(args.pin)]['crons'].append({
			'name': args.cron,
			'action': args.output == 'high'
			})

		logging.debug('commands/crons.py, add: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Added cron {} to pin {}'.format(args.cron, args.pin))


def disable(args):
	'''
	FUNCTION NAME: disable

	PURPOSE: Disables pin.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Varible	Type		Description
	# -------	----		-----------
	# crons		dict		Data from crons.json.
	# pin_status	boolean		Pin Status. (Enabled or disabled)
	# pin_data	dict		Data from pin.
	# cron_output	str		High or Low. Pin output on cronjob trigger.

	crons = {}

	# Check arguments
	check_args(args, 'disable')

	if args.action != 'disable':
		log_error_and_exit(wrong_action_handler,
				'commands/cron.py, disable: wrong action handlers.',
				1)

	# Check required arguments
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, disable')
	check_cronfile(args.cronfile, 'commands/cron.py, disable')

	# Log unused arguments
	log_output_arg_unused(args.output, 'disable')
	log_cron_arg_unsed(args.cron, 'disable')
	log_name_arg_unsed(args.name, 'disable')

	logging.debug('commands/crons.py, disable: Opening crons file.')
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/crons.py, disable')

		pin_status = find_pin_status(crons, args.pin)

		if pin_status is None:
			pin_not_found_die(args.pin, 'NAME', 'disable')
		
		if type(pin_status) != str:
			logger.error('command/cron.py, disable: find_pin_status returned an invalid type.')
			pin_not_found_die(args.pin, args.name, 'disable')

		if pin_status == 'disabled':
			print('Pin {} is already disabled.').format(args.pin)
			return

		# Disabled pin

		for cron in crons[pin_status][str(args.pin)]['crons']:

			cron_output = 'high' if cron['action'] == True else 'low'

			if remove_cron(cron['name'], '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, args.pin, cron_output)) == False:
					log_error_and_exit(
						Exception(
							'Failed to remove cronjob.'
							),
							'commands/cron.py, remove: remove_cron returned a bad status.',
							1
						)

		pin_data = crons['enabled'].pop(str(args.pin))
		crons['disabled'][str(args.pin)] = pin_data

		logging.debug('commands/crons.py, disable: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Pin {} disabled.'.format(args.pin))


def enable(args):
	'''
	FUNCTION NAME: enable

	PURPOSE: Enables pin.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Varible	Type		Description
	# -------	----		-----------
	# crons		dict		Data from crons.json.
	# pin_status	boolean		Pin Status. (Enabled or disabled)
	# pin_data	dict		Data from pin.
	# cron_output	str		High or Low. Pin output on cronjob trigger.

	crons = {}

	# Check arguments
	check_args(args, 'enable')

	if args.action != 'enable':
		log_error_and_exit(wrong_action_handler,
				'commands/cron.py, enable: wrong action handlers.',
				1)

	# Check required arguments
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, enable')
	check_cronfile(args.cronfile, 'commands/cron.py, enable')

	# Log unused arguments
	log_output_arg_unused(args.output, 'enable')
	log_cron_arg_unsed(args.cron, 'enable')
	log_name_arg_unsed(args.name, 'enable')

	logging.debug('commands/crons.py, enable: Opening crons file.')
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('command/cron.py, enable')

		pin_status = find_pin_status(crons, args.pin)

		if pin_status is None:
			pin_not_found_die(args.pin, 'NAME', 'enable')

		if type(pin_status) != str:
			logger.error('command/cron.py, enable: find_pin_status returned an invalid type.')
			pin_not_found_die(args.pin, args.name, 'enable')

		if pin_status == 'enabled':
			print('Pin {} is already enabled.').format(args.pin)
			return

		# Enable pin
		for cron in crons[pin_status][str(args.pin)]['crons']:

			cron_output = 'high' if cron['action'] == True else 'low'

			if install_cron(str(cron['name']), '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, args.pin, cron_output)) == False:
				log_error_and_exit(
						Exception('Could not add cronjob. Bad syntax?'),
						'commands/cron.py, add: bad status from install_cron.',
						1
						)


		pin_data = crons['disabled'].pop(str(args.pin))
		crons['enabled'][str(args.pin)] = pin_data

		logging.debug('commands/crons.py, enable: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Pin {} enabled.'.format(args.pin))


def rename(args):
	'''
	FUNCTION NAME: rename

	PURPOSE: Handles the rename action.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# crons		dict	Data from crons.json
	# pin_status	str	Pin status. (enabled or disabled).

	crons = {}

	# Check arguments
	check_args(args, 'rename')

	if args.action != 'rename':
		log_error_and_exit(wrong_action_handler,
				'commands/cron.py, rename: wrong action handler.',
				1)

	# Check required arguments
	check_name(args.name, 'rename')
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, rename')
	check_cronfile(args.cronfile, 'commands/cron.py, rename')

	# Check for unsed system arguments
	log_output_arg_unused(args.output, 'rename')
	log_cron_arg_unsed(args.cron, 'rename')

	logging.debug('commands/crons.py, remame: Openning crons file.')
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/cron.py: rename')

		pin_status = find_pin_status(crons, args.pin)

		# Not found- fatal exit.
		if pin_status is None:
			pin_not_found_die(args.pin, args.name, 'rename')

		if type(pin_status) != str:
			logger.error('command/cron.py, rename: find_pin_status returned an invalid type.')
			pin_not_found_die(args.pin, args.name, 'rename')

		crons[pin_status][str(args.pin)]['name'] = args.name;

		# Write crons to file
		logging.debug('commands/crons.py, remame: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Renamed pin {} to {}.'.format(args.pin, args.name))


def init(args):
	'''
	FUNCTION NAME: init

	PURPOSE: Handles the init action.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# crons		dict	Data from crons.json

	# Check arguments
	check_args(args, 'init')

	if args.action != 'init':
		log_error_and_exit(wrong_action_handler,
		'commands/cron.py, init: wrong action handler.',
		1)

	# Check for required system arguments
	check_name(args.name, 'init')
	# These are external utils so they require an absolute log origin.
	check_pin_number(args.pin, 'commands/cron.py, init')
	check_cronfile(args.cronfile, 'commands/cron.py, init')

	# Check for unsed system arguments
	log_output_arg_unused(args.output, 'init')
	log_cron_arg_unsed(args.cron, 'init')

	# Load cronfile
	logging.debug('commands/crons.py, init: Openning crons file.')
	crons = load_cron_file(args.cronfile)
	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/crons.py, init')

		# Check if pin exists
		if (str(args.pin) in crons['disabled'] or
				str(args.pin) in crons['enabled']):
			log_error_and_exit(Exception(
				'Pin exists in crons file.'
				),
				'commands/crons.py init: pin {} exists.'.format(args.pin),
				1)

		# Add the new pin
		crons['disabled'][str(args.pin)] = {
				'name': args.name,
				'crons': []
				}

		# Write the crons file
		logging.debug('commands/crons.py, init: Writing crons file.')
		write_cron_file(args.cronfile, crons)

		logging.info('Init pin {} on {}.'.format(args.pin, args.cronfile))


#
# LOG UNSED ARGUMENTS - HANDLERS
#


def log_name_arg_unsed(name, log_origin):

	'''
	FUNCTION NAME: log_name_arg_unsed

	PURPOSE: Log that the name parameter was ignore if it is defined.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	name		str	Name.
	log_origin	str	Log origin: function of origin.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# checked_origin	str	Validated log_origin.

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, log_name_arg_unsed: invalid origin')
		checked_origin = 'unknown'

	if name is not None:
		logging.info('commands/crons.py {}: --name ignored.'.format(checked_origin))


def log_cron_arg_unsed(cron, log_origin):
	'''
	FUNCTION NAME: log_cron_arg_unsed

	PURPOSE: Log that the cron parameter was ignore if it is defined.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	cron		str	Cron statement.
	log_origin	str	Log origin: function of origin.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# checked_origin	str	Validated log_origin.

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, log_cron_arg_unsed: invalid origin')
		checked_origin = 'unknown'

	if cron is not None:
		logging.info('commands/crons.py {}: --cron ignored.'.format(checked_origin))


def log_output_arg_unused(output, log_origin):
	'''
	FUNCTION NAME: log_output_arg_unused

	PURPOSE: Check if the '--output' parameters was included.
	If so, log it. Use only if the action does not use it.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	output		str	Output parameter.
	log_origin		str	Log origin: Function of origin.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# checked_origin	str	Validated log_origin.

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, log_cron_arg_unsed: invalid origin')
		checked_origin = 'unknown'

	if output is not None:
		logging.info('commands/crons.py {}: --output ignored.'.format(checked_origin))


#
# CHECK REQUIRED ARGUMENTS - FATAL EXITS
#


def check_output(output, log_origin):
	'''
	FUNCTION NAME: check_output

	PURPOSE: Check the value of --output.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	output		str	Pin output - high or low.
	log_origin	str	Log origin: function of origin.

	RETURN VALUE: True
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type		Description
	# --------		----		-----------
	# checked_origin	str		Validated log_origin.
	# except_bad_output	Exception	Bad output exception.

	except_bad_output = Exception('Output is required. [\'high\' or \'low\'].')

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, check_name: invalid origin')
		checked_origin = 'unknown'

	if type(output) != str:
		log_error_and_exit(except_bad_output,
				'commands/cron.py, {}: invalid output type.'.format(checked_origin),
				1)

	if output not in ['high', 'low']:
		log_error_and_exit(except_bad_output,
				'commands/cron.py, {}: output should be high or low - {} not allowed'.format(
					checked_origin, output
					),
				1)

	return True


def check_cron(cron, log_origin):
	'''
	FUNCTION NAME: check_cron

	PURPOSE: Check the cron parameter. Fatal exit if it's invalid.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	cron		str	Cron statement.
	log_origin	str	Log origin: Function origin.

	RETURN VALUE: True
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type		Description
	# --------		----		-----------
	# checked_origin	str		Validated log_origin.
	# except_bad_cron	Exception	Exception triggered when cron statement is invalid.

	except_bad_cron = Exception('Invalid cron.')

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, check_cron: invalid origin')
		checked_origin = 'unknown'

	# Check the cron statement

	if type(cron) != str or len(cron) <= 0:
		log_error_and_exit(except_bad_cron,
				'commands/cron.py, check_cron: wrong cron type.',
				1)

	return True


def check_name(name, log_origin):
	'''
	FUNCTION NAME: check_name

	PURPOSE: Check the name parameter. Fatal exit if it's invalid.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	name		str	Name parameter.
	log_origin	str	Log origin: function of origin.

	RETURN VALUE: True
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# checked_origin	str	Validated log_origin.

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, check_name: invalid origin')
		checked_origin = 'unknown'

	if name is None or len(name) <= 0:
		log_error_and_exit(Exception(
			'--name is required.'
			),
			'commands/crons.py init: --name not defined.',
			1)

	return True


def check_args(args, log_origin):
	'''
	FUNCTION NAME: check_args
	
	PURPOSE: Check that the args namespace has the correct aguments.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.
	log_origin	str			Log origin: function of origin.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# checked_origin	str	Validated log_origin.

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/crons.py, check_args: invalid origin')
		checked_origin = 'unknown'


	if type(args) != argparse.Namespace:
		log_error_and_exit(bad_parse_exept,
			'commands/cron.py, {}: args is an invalid namespace.'.format(checked_origin),
			1)

	# All arguments should be there, even if they are not defined.
	# Extra arguments don't affect the functionality.
	if ('action' not in args or
		'command' not in args or
		'cron' not in args or
		'cronfile' not in args or
		'name' not in args or
		'output' not in args or
		'pin' not in args):
		log_error_and_exit(bad_parse_exept,
				'commands/cron.py, {}: args is an invalid namespace'.format(checked_origin),
				1)

	return True


#
# FATAL EXITS
#


def pin_not_found_die(pin, name, log_origin):
	'''
	FUNCTION NAME: pin_not_found_die

	PURPOSE: Fatal exit. Used after a pin was not found in crons.

	ARGUMENT LIST:

	Argument	Type	Description
	--------	----	-----------
	log_origin	str	Log Origin: Function of origin.
	pin		int	Pin.
	name		str	Pin Name.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	----------
	# checked_log_origin	str	Validated log origin.


	# WARNING: If log_origin is not of type str.
	# Then checked_log_origin will change type to str.
	checked_log_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/cron.py, pin_not_found_die: log_origin is invalid.')
		checked_log_origin = 'unknown'

	# pin and name can be anything. No need to check them.

	log_error_and_exit(Exception(
		'Pin not found. Run cron init --pin {0} --name "{1}"'.format(
			pin, name)),
		'commands/cron.py, {}: Pin {} not found'.format(checked_log_origin, pin),
		1)
