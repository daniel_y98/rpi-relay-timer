# FILE NAME:	reload.py
#
# PURPOSE: Reload Pins.
#
# FILE REFERENCES:
#
# Name		IO	Description
# ---		--	-----------
# state.json	I	Pin state file. Full path provided by system arguments.
# crons.json	IO	Cronjob info file. Full path provided by system arguments.
#
# EXTERNAL VARIABLES:
#
# Source: headers.py
#
# Name			Type	IO	DESCRIPTION
# ----			----	--	-----------
# COMMAND_RELOAD	str	I	Command string for reload.
# PIN_LOCK		str	I	Location of the lock file for pin operations.
# CRON_LOCK		str	I	Locatin of the lock file for cron operations.
#
# EXTERNAL REFERENCES:
#
# Name			DESCRIPTION
# ----			-----------
# argparse		Argument Parser.
# logging		Logging handler.
# log_error_and_exit	Fatal exit log handler.
# gpio_setup		Resets the gpio pins.
# check_statefile	Checks the state file.
# load_state_file	Loads the state file.
# FileLock		Locks a file, and prevents it from being written on twice.
# check_cronfile	Checks the cronfile argument and exists fatally if the input is invalid.
# install_cron		Install a given cronjob in the system.
# remove_cron		Remove a given cronjob from the system.
# FileLock		Locks a file, and prevents it from being written on twice.
#
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES:
#	- Invalid Arguments
#	- Invalid state file.
#	- Invalid cron file.
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS: none

import argparse
import logging
from commands.header import COMMAND_RELOAD, RPI_INSTALL_NAME
from commands.header import PIN_LOCK, CRON_LOCK
from logger import log_error_and_exit
from commands.modules.gpio import gpio_setup
from commands.modules.utils import load_state_file, load_cron_file
from commands.modules.utils import check_statefile, check_cronfile
from commands.modules.cron_ops import install_cron, remove_cron

try:
	from filelock import FileLock
except:
	FileLock = None
	log_error_and_exit(
			Exception('Critical dependency missing - filelock.'),
			'Unable to continue, missing filelock.',
			1)

def reload_command(args):
	'''
	FUNCTION NAME: reload_command

	PURPOSE: Handles the reload command.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments

	RETURN VALUE: None
	'''

	check_args(args, 'reload')

	if args.command != COMMAND_RELOAD:
		log_error_and_exit(Exception(
			'Wrong command handler.'
		),
		'commands/reload.py, reload_command: wrong command handler.',
		1)

	if args.action == 'state':
		state(args)
	if args.action == 'crons':
		crons(args)
	else:
		log_error_and_exit(Exception(
			'No handler for action {}.'.format(args.action)
			),
			'commands/setup.py, setup: no catch for {}.'.format(args.action), 1)


def state(args):
	'''
	FUNCTION NAME: state

	PURPOSE: Handle the state action.

	ARGUMENT LIST:

	Argument	Type			Description
	-------		----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''
	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# state		dict	State data from state.json

	state = {}

	check_args(args, 'reload')

	if args.action != 'state':
		log_error_and_exit(wrong_action_except,
				'commands/reload.py, state: wrong action handler.',
				1)

	check_statefile(args.statefile, 'commands/reload.py, state')

	lock = FileLock(PIN_LOCK, timeout=10)
	with lock:
		state = load_state_file(args.statefile)

		logging.info('Reloading pin state.')
		print('Reloading pin state.')
		gpio_setup(state)


def crons(args):
	'''
	FUNCTION NAME: crons

	PURPOSE: Handle the crons action.

	ARGUMENT LIST:

	Argument	Type			Description
	-------		----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type	Description
	# --------	----	-----------
	# crons		dict	Crons data from crons.json
	# cron_output	str	High or Low. Cron output value.

	crons = {}

	check_args(args, 'reload')

	if args.action != 'crons':
		log_error_and_exit(wrong_action_except,
				'commands/reload.py, crons: wrong action handler.',
				1)

	lock = FileLock(CRON_LOCK, timeout=10)
	with lock:
		check_cronfile(args.statefile, 'commands/reload.py, crons')

		logging.info('Installing crons.')
		print('Cronfile found, installing crons.')

		crons = load_cron_file(args.cronfile)
		if type(crons) != dict or type(crons) == {}:
			invalid_crons_die('commands/setup.py, crons')


		# Remove old crons
		for pin in crons['enabled']:
			for cron in crons['enabled'][pin]['crons']:
				cron_output = 'high' if cron['action'] == True else 'low'
				remove_cron(cron['name'], '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, pin, cron_output))
		for pin in crons['disabled']:
			for cron in crons['disabled'][pin]['crons']:
				cron_output = 'high' if cron['action'] == True else 'low'
				remove_cron(cron['name'], '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, pin, cron_output))

		# Install the crons
		for pin in crons['enabled']:
			for cron in crons['enabled'][str(pin)]['crons']:

				cron_output = 'high' if cron['action'] == True else 'low'

				if install_cron(str(cron['name']), '{0} pin {1} {2}'.format(RPI_INSTALL_NAME, pin, cron_output)) == False:
					log_error_and_exit(
							Exception('Could not add cronjob. Bad syntax?'),
							'commands/cron.py, add: bad status from install_cron.',
							1
							)


def check_args(args, log_origin):
	'''
	FUNCTION NAME: check_args

	PURPOSE: Check that the args namespace has the correct aguments.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.
	log_origin	str			Log origin: function of origin.
	'''

	# LOCAL VARIABLES
	#
	# Variable		Type	Description
	# --------		----	-----------
	# checked_origin	str	Validated log_origin.

	# WARNING: If origin is not of type str, checked_origin will change type.
	checked_origin = log_origin

	if type(log_origin) != str or len(log_origin) <= 0:
		logging.warn('commands/reload.py, check_args: invalid origin')
		checked_origin = 'unknown'


	if type(args) != argparse.Namespace:
		log_error_and_exit(bad_parse_exept,
			'commands/reload.py, {}: args is an invalid namespace.'.format(checked_origin),
			1)

	# All arguments should be there, even if they are not defined.
	# Extra arguments don't affect the functionality.
	if ('action' not in args or
		'command' not in args or
		'cronfile' not in args or
		'statefile' not in args):
		log_error_and_exit(bad_parse_exept,
				'commands/reload.py, {}: args is an invalid namespace'.format(checked_origin),
				1)

	return True
