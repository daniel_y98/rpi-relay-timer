# FILE NAME:	header.py
#
# PURPOSE: Define constant variables used for commands.
#
# GLOBAL VARIABLES:
#
# VARIABLE		Type		Description
# --------		----		-----------
# COMMAND_RELOAD	str		Command string for reload.
# COMMAND_SETUP		str		Command string for setup.
# COMMAND_PIN		str		Command string for pin.
# COMMAND_CRON		str		Command string for cron.
# VALID_COMMANDS	list[str]	Valid commands list.
# wrong_action_except	Exception	Wrong action exception.
# bad_parse_exept	Exception	Bad parse exception.
# RPI_INSTALL_NAME	str		Installation name.
# WRITE_LOCK		str		Location of the lock file for write.
# PIN_LOCK		str		Location of the lock file for pin operations.
# CRON_LOCK		str		Location of the lock file for cron operations.

COMMAND_RELOAD	= 'reload'
COMMAND_SETUP	= 'setup'
COMMAND_PIN	= 'pin'
COMMAND_CRON	= 'cron'

VALID_COMMANDS = [
		COMMAND_PIN,
		COMMAND_RELOAD,
		COMMAND_SETUP,
		COMMAND_CRON
		]

wrong_action_except = Exception('Wrong action handler.')
bad_parse_exept = Exception('Failed to parse arguments.')

RPI_INSTALL_NAME = 'rpi_timer'

WRITE_LOCK = '/var/lib/rpi_timer/rpi_timer.write.lock'
PIN_LOCK = '/var/lib/rpi_timer/rpi_timer.pin.lock'
CRON_LOCK = '/var/lib/rpi_timer/rpi_timer.cron.lock'
# WRITE_LOCK = 'rpi_timer.write.lock'
# PIN_LOCK = 'rpi_timer.pin.lock'
# CRON_LOCK = 'rpi_timer.cron.lock'
