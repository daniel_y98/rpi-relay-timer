# FILE NAME:	pin.py
#
# PURPOSE: Manage cronjobs.
#
# FILE REFERENCES:
#
# Name		IO	Description
# ---		--	-----------
# state.json	IO	Pin state file. Full path provided by system arguments.
#
# EXTERNAL VARIABLES:
#
# Source: header.py
#
# Name			Type	IO	DESCRIPTION
# ----			----	--	-----------
# COMMAND_PIN		str	I	Command string for setup.
# PIN_LOCK		str	I	Location of the lock file for pin operations.
#
# EXTERNAL REFERENCES:
#
# Name			DESCRIPTION
# ----			----------
# argparse		Argument Parser.
# log_error_and_exit	Fatal log and exit handler.
# json			JSON encoder & decoder.
# logging		Logging handler.
# load_state_file	Loads the state file.
# gpio_setup		GPIO setup.
# gpio_set_pin_low	Sets the given pin to the low state.
# gpio_set_pin_high	Sets the given pin to high state.
# FileLock		Locks a file, and prevents it from being written on twice.
#
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES:
#	- Invalid Arguments
#	- Invalid state file.
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS: none

import argparse
import json
import logging
from logger import log_error_and_exit
from commands.header import COMMAND_PIN, PIN_LOCK
from commands.modules.gpio import gpio_setup, gpio_set_pin_low
from commands.modules.gpio import gpio_set_pin_high
from commands.modules.utils import load_state_file, write_state_file

try:
	from filelock import FileLock
except:
	FileLock = None
	log_error_and_exit(
			Exception('Critical dependency missing - filelock.'),
			'Unable to continue, missing filelock.',
			1)

def pin(args):
	'''
	FUNCTION NAME: pin

	PURPOSE: Handles the pin command.

	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	Command Arguments.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type		Description
	# --------	----		----------
	# state		dict		Pin state.
	# state_file	file_stream	State file stream.

	state =  {}


	# Check arguments
	if type(args) != argparse.Namespace:
		log_error_and_exit(Exception(
			'Failed to parse arguments.'
			),
			'commands/pin.py, pin: args is an invalid namespace.',
			1)
	if args.command != COMMAND_PIN:
		log_error_and_exit(Exception(
			'Wrong command handler.'
		),
		'commands/pin.py, pin: wrong command handler.',
		1)
	if type(args.pin) != int or args.pin < 0 or args.pin > 40:
		log_error_and_exit(Exception(
				'Invalid pin number. Choose one between 0 to 40.'
			),
			'commands/pin.py, pin: pin number out of bounds.',
			1)

	lock = FileLock(PIN_LOCK, timeout=10)
	with lock:

		state = load_state_file(args.statefile)

		if type(state) != dict:
			log_error_and_exit(
					Exception('Failed to load state file')
					,
					'commands/pin.py, pin: load_state_file returned an invalid state.',
					1)

		# Add the pin to state if it's not there
		if str(args.pin) not in state:
			state[str(args.pin)] = 'true' if args.action == 'high' else 'true'

		# Setup pins
		if gpio_setup(state) is not True:
			logging.warn('commands/pin.py, pin: Pins not setup, nothing to do.')
			return

		gpio_state = {}

		if args.action == 'high':
			gpio_state = gpio_set_pin_high(args.pin, state)
		elif args.action == 'low':
			gpio_state = gpio_set_pin_low(args.pin, state)
		else:
			log_error_and_exit(Exception('Invalid action.'),
					'commands/pin.py, pin: no handler for action {}'.format(args.action),
					1)

		if type(gpio_state) != dict or gpio_state == {}:
			logging.error('commands/pin.py, pin: GPIO unsuccessful. Possibly broken state.')
			return

		state = gpio_state
		logging.info('Pin {} output set to {}.'.format(args.pin, args.action))
		print('Pin {} output set to {}.'.format(args.pin, args.action))

		logging.debug('Writing the state file.')
		write_state_file(args.statefile, state)
