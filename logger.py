# FILE NAME:	setup.py
#
# PURPOSE: Provide commun loggin interface.
#
# Name			IO	Description
# ---			--	-----------
# rpi_timer.log		O	Log file
#
# EXTERNAL VARIABLES:
#
# Source: logger_header.py
#
# Name		Type	IO	DESCRIPTION
# ----		----	--	-----------
# LOGGER	str	I	Main logger name.
# logger_level	int	I	Logger level.
#
# EXTERNAL REFERENCES:
#
# Name		DESCRIPTION
# ----		-----------
# logging	Logger
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES: none
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS: none

import logging
from logger_header import LOGGER, logger_level

def init_logger():
	'''
	FUNCTION NAME: init_logger

	PURPOSE: Initializes logging

	ARGUMENT LIST:

	Name	Type	Description
	----	----	-----------
	none

	RETURN VALUE: None
	'''
	logging.basicConfig(
		filename=LOGGER,
		filemod='a',
		format='%(asctime)s,%(msecs)d %(levelname)s %(message)s',
		datefmt='%H:%M:%S',
		level=logger_level
	)

def log_error_and_exit(exception, message, code):
	'''
	FUNCTION NAME: log_error_and_exit

	PURPOSE: Logs error, and exits with an exception.

	ARGUMENT LIST:

	Name		Type		Description
	----		----		-----------
	exception	Exception	Exception to print.
	message		str		Message to log.
	code		int		Exit code.

	RETURN VALUE: None
	'''
	# LOCAL VARIABLES
	#
	# Variable		Type				Description
	# --------		----				----------
	# bad_logger_except	Exception			Exception.
	# code_checked_val	int				Final exit code.

	bad_logger_except = Exception('Failed to log exit.')
	code_checked_val = 1

	# Check Arguments
	if type(exception) != Exception:
		logging.error('logger.py, log_error_and_exit: Bad Exception provided.')
		print(bad_logger_except)
		exit(1)
	if type(message) != str or len(message) < 0:
		logging.error('logger.py, log_error_and_exit: Bad message provided.')
		print(bad_logger_except)
		# Print original exception
		print(exception)
		exit(1)
	if type(code) != int or code == 0:
		logging.warn('logger.py, log_error_and_exit: Invalid exit code, falling back to 1')
	else:
		code_checked_val = code

	logging.error(message)
	print(exception)
	exit(code_checked_val)
