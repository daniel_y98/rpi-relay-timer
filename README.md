# RPI Relay Timer

This is a quick and dirty relay timer for the raspberry pi 3B+. It's basically just a cli that install cronjobs.

## Installation

Clone the repository in ``/opt``.

```bash
cd /opt
git clone https://gitlab.com/daniel_y98/rpi-relay-timer.git
```

Install the dependencies.

```bash
cd rpi-relay-timer
pip install -r requirements.txt
```

Create a symlink for the python executable.

```bash
ln -s /opt/rpi-relay-time/rpi_timer.py /usr/bin/rpi_timer
```

Create the logs directory

```bash
mkdir /var/log/rpi_timer/
```

Create the data directory

```bash
mkdir /var/lib/rpi_timer
```

If you want the pin state to be restored after a reboot, add the following to root's crontab.

```
@reboot rpi_timer reload state
```

## CLI
**rpi__timer**

``setup``
Sets up the state or crons file, also restores them if they already exist.

Example:

```bash
rpi_timer setup crons
rpi_timer setup state
```

``reload``
Restores the pin state or crons. This requires the data files to exist.

Example:

```bash
rpi_timer reload crons
rpi_timer reload state
```

``pin``
Crons the pins.

Example:

```bash
rpi_timer pin 6 high
rpi_timer pin 2 low
```

``cron``
Controls the cronjobs and the main data file (``crons.json``). It has multiple actions:

```bash
rpi_timer cron init --pin 2 --name "Relay 2" # This will initialize pin 2 in the mail data file.

rpi_timer cron enable --pin 2 # This will enable pin 2 and install it's crons (if any)

rpi_timer cron disable --pin 2 # This will disable pin 2 and uninstall it's crons (if any).

rpi_timer cron add --pin 3 --cron "* * * * *" --output high # This will add a cronjob to pin 3. The cronjob will set pin 3 to high. 

rpi_timer cron delete --pin 3 --cron "* * * * *" # This will delete th given cronjob.
```

## Files
**/var/log/rpi_timer/rpi_timer.log**
The log file.

**/var/lib/rpi_timer/_crons.json**
Main data file. This file has all the cronjobs, pin status, pin name, etc.

**/var/lib/rpi_timer/state.json**
State file. This file has the state information for each pin (It keeps track of which ones are high and which ones are low).
