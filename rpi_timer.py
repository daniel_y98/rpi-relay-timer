#!/usr/bin/python2

# FILE NAME:	rpi_timer.py
#
# Relay control through cronjobs for the Raspberry Pi 3 Model B+.
#
# FILE REFERENCES:
#
# Name	IO	Description
# ---	--	-----------
# none
#
# EXTERNAL VARIABLES:
#
# Source: commands/header.py
#
# Name			Type		IO	Description
# ----			----		--	-----------
# COMMAND_RELOAD	str		I	Command string for reload.
# COMMAND_SETUP		str		I	Command strig for setup.
# COMMAND_PIN		str		I	Command string for pin.
# VALID_COMMANDS	list[str]	I	Valid command list.
#
# EXTERNAL REFERENCES:
#
# Name			DESCRIPTION
# ----			-----------
# reload_command	Handles the reload command.
# setup			Handles the setup command.
# pin			Handles the pin command.
# cron			Handles the cron command.
# argparser		Handles cli argument parsing.
# init_logger		Initializes logging.
# init_parser		Initializes the argument parser.
# get_sys_args		Gets the command line arguments.
# log_error_and_exit	Handles error login & fatal exits.
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES: none
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS:
# - Running on a raspberry pi 3b+.
# - Superuser permissions needed.

import argparse
from logger import init_logger, log_error_and_exit
from args import init_parser, get_sys_args
from commands.header import *
from commands.reload import reload_command
from commands.setup import setup
from commands.pin import pin
from commands.cron import cron

# Exit message (when something goes wrong)
except_bad_parse = Exception('Failed to parse arguments.')

def main():
	# LOCAL VARIABLES
	#
	# Variable		Type				Description
	# --------		----				----------
	# parser		argparser.ArgumentParser	Argument Parser
	# sys_args		argparse.Namespace		Arguments

	# Initialize logging
	init_logger()

	# Argument Parsing
	parser = init_parser()
	if type(parser) != argparse.ArgumentParser:
		log_error_and_exit(except_bad_parse,
				'rpi_timer.py, main: init_parser returned an invalid parser.',
				1)

	sys_args = get_sys_args(parser)
	if type(sys_args) != argparse.Namespace:
		log_error_and_exit(except_bad_parse,
				'rpi_timer.py, main: get_sys_args returned an invalid namespace.',
				1)

	if sys_args.command not in VALID_COMMANDS:
		log_error_and_exit(except_bad_parse,
				'rpi_timer.py, main: invalid command provided by parser.',
				1)

	# Command Switch - This is where command handling happens.
	command_switch(sys_args)

def command_switch(args):
	'''
	FUNCTION NAME: command_switch



	ARGUMENT LIST:

	Argument	Type			Description
	--------	----			-----------
	args		argparse.Namespace	System Arguments

	RETURN VALUE: None
	'''

	# Check arguments
	if type(args) != argparse.Namespace:
		log_error_and_exit(except_bad_parse,
				'rpi_timer.py, command_switch: invalid argument namespace.',
				1)
	if args.command not in VALID_COMMANDS:
		log_error_and_exit(except_bad_parse,
				'rpi_timer.py, command_switch: invalid command.',
				1)

	if args.command == COMMAND_SETUP:
		setup(args)
	elif args.command == COMMAND_RELOAD:
		reload_command(args)
	elif args.command == COMMAND_PIN:
		pin(args)
	elif args.command == COMMAND_CRON:
		cron(args)
	else:
		log_error_and_exit(Exception(
			'Command has not handler.'
			),
			'rpi_timer.py, command_switch: no command catch.',
			1)

# Call the main function
if __name__ == "__main__":
	main()


