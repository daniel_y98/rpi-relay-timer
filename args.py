# FILE NAME:	args.py
#
# PURPOSE: Argument parser.
#
# FILE REFERENCES:
#
# Name	IO	Description
# ---	--	-----------
# none
#
# EXTERNAL VARIABLES:
#
# Source: commands/header.py
#
# Name			Type		IO	DESCRIPTION
# ----			----		--	-----------
# COMMAND_SETUP		str		I	Command string for setup.
# COMMAND_RELOAD	str		I	Command string for reload.
# COMMAND_PIN		str		I	Command string for pin.
# VALID_COMMANDS	list[str]	I	Valid commands list.
#
# EXTERNAL REFERENCES:
#
# Name			DESCRIPTION
# ----			----------
# argparse		Argument Parser.
# log_error_and_exit	Fatal exit handler.
#
#
# ABNORMAL TERMINATION CONDITIONS, ERROR AND WARNING MESSAGES: none
#
# NOTES: none
#
# ASSUMPTIONS, CONSTRAINTS, RESTRICTIONS: none

import argparse
from logger import log_error_and_exit
from commands.header import *


# Exit message (when something goes wrong)
except_bad_parse = Exception('Failed to parse arguments.')


def init_parser():
	'''
	FUNCTION NAME: init_parser

	PURPOSE: Initializes the argument parser.

	ARGUMENT LIST:

	Argument	Type	IO	Description
	--------	----	--	-----------
	none

	RETURN VALUE: (argparse.ArgumentParser) Parser
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type				Description
	# --------	----				-----------
	# parser	argparse.ArgumentParser		Argument Parser
	# subparsers	argparse._SubParsersAction	Sub Parsers

	parser = argparse.ArgumentParser(description='''
		Relay control through cronjobs for the Raspberry Pi 3 Model B+
	''')
	subparsers = parser.add_subparsers(help='Commands')

	init_reload_parser(subparsers)
	init_setup_parser(subparsers)
	init_pin_parser(subparsers)
	init_cron_parser(subparsers)

	if type(parser) != argparse.ArgumentParser:
		log_error_and_exit(except_bad_parse,
				'args.py, init_parser: argparse.ArgumentParser returned an invalid parser.',
				1)

	return parser


def init_setup_parser(subparsers):
	'''
	FUNCTION NAME: init_setup_parser

	PURPOSE: Initializes the setup parser.

	ARGUMENT LIST:

	Name		Type				Description
	----		----				-----------
	subparsers	argparse._SubParsersAction	Subparser Object

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type				Description
	# --------	----				-----------
	# setup_parser	argparse.ArgumentParser		Setup Parser
	#

	if type(subparsers) != argparse._SubParsersAction:
		log_error_and_exit(except_bad_parse,
				'args.py, init_setup_parser: Invalid parser provided.',
				1)

	setup_parser = subparsers.add_parser(COMMAND_SETUP,
			help='Setup timer.')
	# Only allow state, and crons. Anything else
	# is rejected.
	setup_parser.add_argument('action',
			choices=['state','crons'],
			help='Action.')
	# This is where the pin state is saved.
	setup_parser.add_argument('-s', '--statefile',
			type=str,
			default='/var/lib/rpi_timer/state.json',
			help='Where the state should be saved.')
	# This is where the cronjob information is kept.
	setup_parser.add_argument('-c', '--cronfile',
			type=str,
			default='/var/lib/rpi_timer/crons.json',
			help='Where the cronjobs should be saved.')
	setup_parser.set_defaults(command='setup')


def init_reload_parser(subparsers):
	'''
	FUNCTION NAME: init_reload_parser

	PURPOSE: Initializes the reload parser.

	ARGUMENT LIST:

	Name		Type				Description
	----		----				-----------
	subparsers	argparse._SubParsersAction	Subparser Object

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type				Description
	# --------	----				-----------
	# reload_parser	argparse.ArgumentParser		Pin Parser
	#

	if type(subparsers) != argparse._SubParsersAction:
		log_error_and_exit(except_bad_parse,
				'args.py, init_reload_parser: Invalid parser provided.',
				1)

	reload_parser = subparsers.add_parser(COMMAND_RELOAD,
			help='Reload.')
	# Only allow state, and crons. Anything else
	# is rejected.
	reload_parser.add_argument('action',
			choices=['state','crons'],
			help='Action.')
	reload_parser.add_argument('-s', '--statefile',
			type=str,
			default='/var/lib/rpi_timer/state.json',
			help='Pin state file path')
	# This is where the cronjob information is kept.
	reload_parser.add_argument('-c', '--cronfile',
			type=str,
			default='/var/lib/rpi_timer/crons.json',
			help='Where the cronjobs should be saved.')
	reload_parser.set_defaults(command='reload')


def init_pin_parser(subparsers):
	'''
	FUNCTION NAME: init_pin_parser

	PURPOSE: Initializes the pin parser.

	ARGUMENT LIST:

	Name		Type				Description
	----		----				-----------
	subparsers	argparse._SubParsersAction	Subparser Object.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type				Description
	# --------	----				-----------
	# pin_parser	argparse.ArgumentParser		Pin Parser.
	#

	if type(subparsers) != argparse._SubParsersAction:
		log_error_and_exit(except_bad_parse,
				'args.py, init_pin_parser: Invalid parser provided.',
				1)

	pin_parser = subparsers.add_parser(COMMAND_PIN,
			help='Pin operations.')
	# Only allow state, and crons. Anything else
	# is rejected.
	pin_parser.add_argument('pin',
			type=int,
			help='Pin number')
	pin_parser.add_argument('action',
			choices=['high', 'low'],
			help='Pin action.')
	pin_parser.add_argument('-s', '--statefile',
			type=str,
			default='/var/lib/rpi_timer/state.json',
			help='Pin state file path')
	pin_parser.set_defaults(command='pin')


def init_cron_parser(subparsers):
	'''
	FUNCTION NAME: init_cron_parser

	PURPOSE: Initializes the cron parser.

	ARGUMENT LIST:

	Name		Type				Description
	----		----				-----------
	subparsers	argparse._SubParsersAction	Subparser Object.

	RETURN VALUE: None
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type				Description
	# --------	----				-----------
	# cron_parser	argparse.ArgumentParser		Cron Parser.
	#

	if type(subparsers) != argparse._SubParsersAction:
		log_error_and_exit(except_bad_parse,
				'args.py, init_cron_parser: Invalid parser provided.',
				1)

	cron_parser = subparsers.add_parser(COMMAND_CRON,
			help='Cron job management.')
	# Only allow state, and crons. Anything else
	# is rejected.
	cron_parser.add_argument('action',
			choices=[
				'add',
				'remove',
				'enable',
				'disable',
				'init',
				'rename',
				'edit'
				],
			help='Action')
	cron_parser.add_argument('-p', '--pin',
			type=int, help="Pin.",
			required=True)

	# Only used for 'add'
	cron_parser.add_argument('-o', '--output',
			choices=['high', 'low'],
			help='Pin output. Only used for \'add\' & \'edit\'.')

	# Used for 'rename' and 'init'
	cron_parser.add_argument('-n', '--name',
			type=str,
			help='Pin name. Used for \'rename\' & \'init\'.')

	# Used for 'add' and 'remove'
	cron_parser.add_argument('-a', '--cron',
			type=str,
			help='Cron statement. Only used for \'add\' & \'remove\'.')

	cron_parser.add_argument('-c', '--cronfile',
			type=str,
			default='/var/lib/rpi_timer/crons.json',
			help='Cron state file path')
	cron_parser.set_defaults(command='cron')


def get_sys_args(parser):
	'''
	FUNCTION NAME: get_sys_args

	PURPOSE: Gets the command line arguments.

	ARGUMENT LIST:

	Argument	Type				Description
	--------	----				-----------
	parser		argparse.ArgumentParser		Parser.

	RETURN VALUE: (argparse.Namespace)
	'''

	# LOCAL VARIABLES
	#
	# Variable	Type			Description
	# --------	----			-----------
	# arguments	argparse.Namespace	Command line arguments.
	#

	# Check function arguments
	if type(parser) != argparse.ArgumentParser:
		log_error_and_exit(except_bad_parse,
				'args.py, get_sys_args: Invalid parser provided.',
				1)

	arguments = parser.parse_args()

	if type(arguments) != argparse.Namespace:
		log_error_and_exit(except_bad_parse,
				'args.py, get_sys_args: Invalid namespace from parser.parse_args().',
				1)
	if arguments.command not in VALID_COMMANDS:
		log_error_and_exit(except_bad_parse,
				'args.py, get_sys_args: Invalid command from parser.parse_args().',
				1)

	return arguments
