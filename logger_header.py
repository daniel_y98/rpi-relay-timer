# FILE NAME:	logger_header.py
# PURPOSE: Provide the main logger name.
#
# EXTERNAL REFERENCES:
#
# Name		IO	Description
# ----		--	-----------
# logging	I	Logging
#
# GLOBAL VARIABLES:
#
# VARIABLE	Type	Description
# --------	----	-----------
# LOGGER	str	Main logger name.
# logger_level	int	Logging level.

import logging

LOGGER		= '/var/log/rpi_timer/rpi_timer.log'
#LOGGER		= 'rpi_timer.log'
logger_level	= logging.DEBUG

